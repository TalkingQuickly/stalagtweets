function Controller() {
    function selectTag(e) {
        app.selectedTag = $.label.text;
        var controller = Alloy.createController("game");
        controller.getView().open();
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    $model = arguments[0] ? arguments[0].$model : null;
    var $ = this, exports = {}, __defers = {};
    $.__views.index = A$(Ti.UI.createWindow({
        backgroundColor: "black",
        backgroundImage: "./world_bg.png",
        id: "index"
    }), "Window", null);
    $.addTopLevelView($.__views.index);
    $.__views.label = A$(Ti.UI.createLabel({
        width: Ti.UI.SIZE,
        height: Ti.UI.SIZE,
        color: "#000",
        fontSize: 15,
        text: "#Testing",
        id: "label"
    }), "Label", $.__views.index);
    $.__views.index.add($.__views.label);
    selectTag ? $.__views.label.on("click", selectTag) : __defers["$.__views.label!click!selectTag"] = !0;
    exports.destroy = function() {};
    _.extend($, $.__views);
    $.index.open();
    __defers["$.__views.label!click!selectTag"] && $.__views.label.on("click", selectTag);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._, A$ = Alloy.A, $model;

module.exports = Controller;