function Controller() {
    function start(e) {
        dead = !1;
        renderFrame();
        spawnSprite();
    }
    function renderFrame() {
        if (dead) return !1;
        for (i in dynamicSprites) dynamicSprites[i].render();
        timer = setTimeout(function() {
            renderFrame();
        }, frameRate);
    }
    function spawnSprite() {
        if (dead) return !1;
        thisSprite = new sprite;
        $.world.add(thisSprite.view);
        dynamicSprites.push(thisSprite);
        timer = setTimeout(function() {
            spawnSprite();
        }, spawnRate);
    }
    function addScore() {
        score += 1;
        $.title.text = "Score: " + score;
    }
    require("alloy/controllers/BaseController").apply(this, Array.prototype.slice.call(arguments));
    $model = arguments[0] ? arguments[0].$model : null;
    var $ = this, exports = {}, __defers = {};
    $.__views.world = A$(Ti.UI.createWindow({
        backgroundColor: "black",
        backgroundImage: "./world_bg.png",
        id: "world"
    }), "Window", null);
    $.addTopLevelView($.__views.world);
    $.__views.title = A$(Ti.UI.createLabel({
        width: Ti.UI.SIZE,
        height: 40,
        color: "#000",
        top: 0,
        fontSize: 15,
        text: "Score: 0",
        id: "title"
    }), "Label", $.__views.world);
    $.__views.world.add($.__views.title);
    start ? $.__views.title.on("click", start) : __defers["$.__views.title!click!start"] = !0;
    exports.destroy = function() {};
    _.extend($, $.__views);
    dead = !1;
    paused = !0;
    score = 0;
    var gravity = 0.5, frameRate = 100, spawnRate = 500, dynamicSprites = new Array, staticSprites = new Array, worldHeight = Ti.Platform.displayCaps.platformHeight - 40, spriteIndex = 0, sprite = function() {
        this.id = "id_" + spriteIndex++;
        var size = 100 - (staticSprites.length + dynamicSprites.length) * 5;
        size = size < 5 ? 5 : size;
        var left = Math.random() * (Ti.Platform.displayCaps.platformWidth - (size + 3));
        left = left < 3 ? 3 : left;
        this.velocity = 0;
        this.view = Ti.UI.createView({
            backgroundColor: "#89525E",
            backgroundImage: "./sprite_bg.png",
            width: size,
            height: size,
            top: worldHeight - size,
            left: left,
            text: this.id
        });
        this.view.addEventListener("click", function() {
            if (!this.active) return !1;
            for (i in dynamicSprites) if (dynamicSprites[i].id == this.id) {
                addScore();
                dynamicSprites[i].kill(!1);
                break;
            }
        });
        this.view.id = this.id;
        this.view.active = !0;
        this.render = function() {
            this.velocity += gravity;
            this.view.setTop(this.view.top - this.velocity);
            if (this.view.top < 0) {
                this.view.setTop(0);
                this.kill(!0);
            }
            for (i in staticSprites) {
                thatView = staticSprites[i].view;
                thatL = thatView.left;
                thatR = thatL + thatView.width;
                thisL = this.view.left;
                thisR = thisL + this.view.width;
                if (staticSprites[i].id != this.id && (thatL >= thisL && thatL < thisR || thatR >= thisL && thatR < thisR) && this.view.top <= thatView.top + thatView.height) {
                    this.view.setTop(thatView.top + thatView.height);
                    this.kill(!0);
                }
            }
        };
        this.kill = function(addToStaticArray) {
            if (addToStaticArray) {
                staticSprites.push(this);
                this.view.active = !1;
                this.view.animate({
                    backgroundColor: "#333",
                    duration: 500
                });
            } else $.world.remove(this.view);
            for (i in dynamicSprites) dynamicSprites[i].id == this.id && dynamicSprites.splice(i, 1);
        };
    };
    __defers["$.__views.title!click!start"] && $.__views.title.on("click", start);
    _.extend($, exports);
}

var Alloy = require("alloy"), Backbone = Alloy.Backbone, _ = Alloy._, A$ = Alloy.A, $model;

module.exports = Controller;