// ===================================================
//           Set Global parameters
// ===================================================

dead = false;
paused = true;
score = 0;

// world variables
var gravity = 0.5;
var frameRate = 100;

// tie the spawn rate to the difficulty level
var spawnRate = 500;// - (app.diffLevel * 2) + 10;

// create the sprites arrays
var dynamicSprites = new Array();
var staticSprites = new Array();

// determine the world height - can't figure this one out
var worldHeight = Ti.Platform.displayCaps.platformHeight - 40;

var spriteIndex = 0;

// ===================================================
//                controllers
// ===================================================

function start(e) {
	//alert($.title.text);
	dead = false;
	renderFrame();
	spawnSprite();
}

function renderFrame(){
	
	if(dead){
		return false;
	}
	
	for(i in dynamicSprites){
		dynamicSprites[i].render();
	}
	timer = setTimeout(function(){renderFrame()},frameRate)
}

// start spriteSpawning
function spawnSprite() {
	
	if(dead){
		return false;
	}

	// create a new sprite and add to world view
	thisSprite = new sprite();
	$.world.add(thisSprite.view);
	dynamicSprites.push(thisSprite);
	
	timer = setTimeout(function(){spawnSprite()},spawnRate)
}

function addScore(){
	// update to diff level soon
	score = score + 1;
	$.title.text = 'Score: '+score;
}

// ===================================================
//                Sprites
// ===================================================

var sprite = function() {

	// ========= Set initial values ==========

	this.id = 'id_'+spriteIndex++;

	// get correct size - linked to the number of sprites
	var size = (100 - ((staticSprites.length + dynamicSprites.length) * 5));
	size = size < 5 ? 5 : size;

	// spawn a random number position minus size + 3 min from left
	var left = Math.random() * (Ti.Platform.displayCaps.platformWidth - (size + 3));
	left = left < 3 ? 3 : left;

	// set initial velocity
	this.velocity = 0;

	// ======= the view object ======

	// create the view
	this.view = Ti.UI.createView({
		backgroundColor : '#89525E',
		backgroundImage:'./sprite_bg.png',
		width : size,
		height : size,
		top : (worldHeight - size),
		left : left,
		text:this.id
	});

	// bind click events
	this.view.addEventListener('click', function() {
		
		if (!this.active) {
			return false;
		}
		
		// this now refers to view object not class obj
		// fetch the class obj - kill it
		for (i in dynamicSprites) {
			if(dynamicSprites[i].id == this.id) {
				// add to score
				addScore();

				dynamicSprites[i].kill(false);
				break;
			}
		}
	});
	
	this.view.id = this.id;
	this.view.active = true;

	// ========== Rendering methods ============

	// set the new velocity and position the box
	this.render = function() {

		// update the ui
		this.velocity += gravity;
		this.view.setTop(this.view.top - this.velocity);

		// ======= Detect collision =======

		// if sprite has reached the top
		if (this.view.top < 0) {
			this.view.setTop(0);
			// ensure the sprite is located properly

			// kill sprite, add to static array
			this.kill(true);
		}

		// loop through static sprites and detect collision
		for (i in staticSprites) {
			thatView = staticSprites[i].view;
			thatL = thatView.left;
			thatR = thatL + thatView.width;
			thisL = this.view.left;
			thisR = thisL + this.view.width;
			
			// sorry about this clause - eurgh
			if (
				staticSprites[i].id != this.id &&
				((thatL >= thisL && thatL < thisR) || (thatR >= thisL && thatR < thisR)) && 
				this.view.top <= (thatView.top + thatView.height)
			) {
				// set the top to the bottom of the target sprite
				this.view.setTop(thatView.top + thatView.height);
				this.kill(true);
			}
		}
	};

	this.kill = function(addToStaticArray) {
		
		if (addToStaticArray) {
			// add to static
			staticSprites.push(this);
			// flag to disable click event
			this.view.active = false;
			// update the view
			this.view.animate(
		    {
		      backgroundColor:'#333',
		      duration:500
		    });
		}else{
			// not adding to static, remove from view
			$.world.remove(this.view);
		}

		// loop dynamic sprites and remove, matching by id
		for (i in dynamicSprites) {
			if (dynamicSprites[i].id == this.id) {
				dynamicSprites.splice(i, 1);
			}
		}
	};

};

