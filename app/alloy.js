// The contents of this file will be executed before any of
// your view controllers are ever executed, including the index.
// You have access to all functionality on the `Alloy` namespace.
//
// This is a great place to do any initialization for your app
// or create any global variables/functions that you'd like to
// make available throughout your app. You can easily make things
// accessible globally by attaching them to the `Alloy.Globals`
// object. For example:
//
// Alloy.Globals.someGlobalFunction = function(){};


// ============================================
//       setup out app with reqrd vars
// ============================================


app = {};



app.selectedTag = 1;
app.diffLevel = 50; // 0 - 100
app.tags = [];

app.updateTags = function(){
	
	var loader = Titanium.Network.createHTTPClient();
	loader.open("GET","http://dev.talkingquickly.co.uk/api/v1/tags.json");  
	loader.onload = function() {
		var data = eval('('+this.responseText+')');  
		app.tags = data.tags;
		app.diffLevel = app.tags[app.selectedTag].tag.difficulty;
	};  
	// Send the HTTP request  
	loader.send();  
}

app.updateTags();
setInterval(function() {
	app.updateTags();
},10000);

 